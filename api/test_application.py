import pytest

from application.app import app, db
from application.database import User, Item

@pytest.fixture
def client():
    # init_DB()
    yield app.test_client()
    truncate_DB()


def truncate_DB():
    with app.app_context():
        Item.query.delete()
        User.query.delete()
        db.session.commit()


def test_index(client):
    response = client.get("/")
    assert response.status_code == 200


def test_index_response(client):
    response = client.get("/")
    assert b"Butorin" in response.data


def test_add_registration(client):
    with app.app_context():
        test_data = {"login": "Mom", "password": "111"}
        client.post("/registration", json=test_data)
        
        test_data = {"login": "Dad", "password": "222"}
        client.post("/registration", json=test_data)
        
        test_data = {"login": "Dad", "password": "444"}
        client.post("/registration", json=test_data)
        
        assert User.query.count() == 2

def test_login(client):
    with app.app_context():
        test_data = {"login": "Cow", "password": "111"}
        client.post("/registration", json=test_data)
        test_data = {"login": "Cow", "password": "111"}
        response = client.post("/login", json=test_data)
        cow_token = response.get_data(as_text=True)
        # assert cow_token == "?"
        cow_user = User.check_user_token(cow_token)
        assert cow_user is not None

def test_add_items(client):
    with app.app_context():
        test_data = {"login": "Dad", "password": "222"}
        client.post("/registration", json=test_data)
        test_data = {"login": "Dad", "password": "222"}
        response = client.post("/login", json=test_data)
        dad_token = response.get_data(as_text=True)

        test_data = {"name": "shirt", "cnt": "5", "token": dad_token}
        client.post("/items/new", json=test_data)
        
        assert Item.query.count() == 1

'''
дальше можно добавить тесты
- список предметов пользователя :  внутри одной процедуры добавить предмет, получить список, проверить что предмет есть в списке
- передачу предмета: полный цикл, - логин юзер-1, добавление предмета, генерация ссылки, логин юзер-2, получение предмета, получение списка, проверить что предмет там есть
.......

поскольку приложение тестовое, не стал сильно на это заморачиваться
'''

if __name__ == "__main__":
    pytest.main()
