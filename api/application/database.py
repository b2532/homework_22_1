from application.app import db
from flask import jsonify
from itsdangerous import TimedJSONWebSignatureSerializer as Signat
from sqlalchemy import CheckConstraint


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(30))
    password = db.Column(db.String(100))
    items = db.relationship('Item', backref='user', lazy='dynamic', uselist=True)

    def __init__(self, login, password) -> None:
        if User.get_user_by_login(login) != None:
            raise Exception(f"Login already exist: {login}")
        if login is None or password is None: 
            raise Exception(f"Login or password incorrect: [{login}]/[{password}]")
        self.login = login
        self.password = password

    @staticmethod
    def get_user_by_login(login):
        usr = User.query.filter_by(login=login).first()
        return usr

    def get_user_token(self, expire_time=1200):
        s = Signat(db.get_app().config['SECRET_KEY'], expires_in=expire_time)
        return s.dumps({'user_id': self.id}).decode('UTF-8')

    @staticmethod
    def check_user_token(token:str):
        s = Signat(db.get_app().config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __str__(self) -> str:
        # return f"{self.id}"
        return jsonify({"id": self.id, "login": self.login, "password": self.password})

# -----------------------------------------------------------------------------

class Item(db.Model):
    __tablename__ = "items"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    cnt = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    __table_args__ = (
        CheckConstraint(cnt >= 0, name='count_of_items_positive'),
        {}
        )

    def __init__(self, name, cnt, user_id) -> None:
        self.name = name
        self.cnt = cnt
        self.user_id = user_id

    @staticmethod
    def delete_by_id(item_id):
        try:
            item2delete = Item.query.get(item_id)

            db.session.delete(item2delete)
            db.session.commit()
        except:
            return ('ERROR, Item was not deleted')

    @staticmethod
    def get_user_items(user_id):
        result = []
        for i in Item.query.filter_by(user_id=user_id).all():
            result.append({'id': i.id, 'name': i.name, 'cnt': i.cnt})
        return jsonify(result)

    @staticmethod
    def change_owner(item_id, new_user_id):
        the_item = Item.query.get(item_id)
        if the_item.user_id != new_user_id:
            the_item.user_id = new_user_id
            db.session.commit()

    def __str__(self) -> str:
        # return f"{self.id}"
        return jsonify({"id": self.id, "name": self.name, "cnt": self.cnt, "user_login": self.user.login})
