from flask_sqlalchemy import SQLAlchemy
from application import create_app

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://23e852b1b1a24965bd26a9b73459b255@o1095196.ingest.sentry.io/6130526",
    integrations=[FlaskIntegration()],

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

db = SQLAlchemy()
app = create_app(db)

with app.app_context():
    db.create_all()
