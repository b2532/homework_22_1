import socket

from flask import Blueprint, config, jsonify, request, url_for

from application.app import db
from application.database import Item, User

view = Blueprint("view", __name__)

def get_hostname():
    return socket.gethostbyname(socket.gethostname())

@view.route("/")
def home():
    return f"Butorin_Mikhail Homework #22-1 {get_hostname()}"

# ------------------------------------------------------------------------------------
@view.route("/registration", methods=["POST"])
def add():
    """ Регистрация пользователя
    • POST-запрос (/registration)
    • параметры запроса – логин ("login"), пароль ("password") пользователя

    Returns:
        сообщение об успешной регистрации
    """
    data = request.json
    try:
        user = User(login=data["login"], password=data["password"])
        db.session.add(user)
        db.session.commit()
        # return jsonify(data)
        return(f'User {data["login"]} registered - {get_hostname()}')
    except Exception as ex:
        return(f'ERROR, User {data["login"]} was not registered: {ex.args[0]}')

# ------------------------------------------------------------------------------------
@view.route("/login", methods=["POST"])
def login():
    """Авторизация пользователя
    • POST-запрос (/login)
    • параметра запроса – логин, пароль пользователя
    
    Returns:
        str: временный токен аутентификации
    """

    data = request.json
    db_user = User.get_user_by_login(data['login'])
    if db_user is None:
        return(f'ERROR, User or password is incorrect: [{data["login"]}]')
    if data['password'] != db_user.password:
        return(f'ERROR, User or password is incorrect: [{data["login"]}]')
    return db_user.get_user_token()
    
# ------------------------------------------------------------------------------------
@view.route("/items/new", methods=["POST"])
def items_new():
    """Создание нового объекта
    • POST-запрос (/items/new)
    • параметры запроса:
    token = временный токен пользователя
    name  = наименование объекта
    cnt   = количество объектов

    Returns:
        str: сообщение об успешном создании объекта, id объекта, атрибуты объекта
    """

    data = request.json

    # сначала проверяем наличие пользователя
    db_user = User.check_user_token(data['token'])
    if db_user is None:
        return('ERROR: User does not exist or token expired. Try to login again')
    
    # добавление объекта
    try:
        item = Item(name=data["name"], cnt=data["cnt"], user_id=db_user.id)
        db.session.add(item)
        db.session.commit()
        # return jsonify(data)
        return(f'Item {data["name"]} successfully added. ID={item.id}')
    except Exception as ex:
        return(f'ERROR, Item {data["name"]} was not added: {ex.args[0]}')

# ------------------------------------------------------------------------------------
@view.route("/items/<item_id>", methods=["DELETE"])
def items_delete(item_id):
    """Удаление объекта
    • DELETE-запрос (/items/:id)
    • параметры запроса – временный токен, id объекта

    Args:
        item_id (int): id объекта

    Returns:
        str: сообщение об успешном удалении объекта
    """

    data = request.json

    # сначала проверяем наличие пользователя
    db_user = User.check_user_token(data['token'])
    if db_user is None:
        return('ERROR: User does not exist or token expired. Try to login again')

    # затем проверяем наличие и принадлежность предмета
    item2delete = Item.query.get(item_id)
    if item2delete is None:
        return ("Item doesn't exist, nothing to delete")
    if item2delete.user_id != db_user.id:
        return("This is not your item, it belongs to another user. Item can be deleted only by owner")

    # и наконец удаление
    try:
        Item.delete_by_id(item_id)
        return('OK Item deleted')
    except Exception as ex:
        return(f'ERROR, Item {data["name"]} was not deleted: {ex.args[0]}')
    
# ------------------------------------------------------------------------------------
@view.route("/items", methods=["GET"])
def items_of_user():
    """Получение списка объектов
    • GET-запрос (/items)
    • параметры запроса – временный токен

    Returns:
        list: список объектов пользователя с их id и атрибутами (каждый объект - словарь)
    """

    data = request.json

    # сначала проверяем наличие пользователя
    db_user = User.check_user_token(data['token'])
    if db_user is None:
        return('ERROR: User does not exist or token expired. Try to login again')

    try:
        return(Item.get_user_items(db_user.id))
    except Exception as ex:
        return(f'ERROR: {ex.args[0]}')

# ------------------------------------------------------------------------------------
@view.route("/send", methods=["POST"])
def send_item():
    """Генерация ссылки для передачи объекта
    • POST-запрос (/send)
    • параметры запроса – id передаваемого объекта, логин принимающего пользователя, временный токен

    Returns:
        str: ссылка, по которой принимающий пользователь должен совершить переход
    """

    data = request.json

    # сначала проверяем наличие пользователя
    db_user = User.check_user_token(data['token'])
    if db_user is None:
        return('ERROR: User does not exist or token expired. Try to login again')

    # затем проверяем наличие и принадлежность предмета
    item2send = Item.query.get(data['item_id'])
    if item2send is None:
        return ("Item doesn't exist, nothing to send")
    if item2send.user_id != db_user.id:
        return("This is not your item, it belongs to another user. Item can be sent only by owner")

    # токен принимающего пользователя
    to_user = User.get_user_by_login(data['to'])
    if to_user is None:
        return(f'ERROR: User [{data["to"]}] does not exist')
    token_user_getter = to_user.get_user_token()

    # и наконец генерация ссылки для передачи
    # send_params = {'from': data['token'], 'to': token_user_getter, 'item': data['item_id'], "_external": True}
    send_params = {'to': token_user_getter, 'item': data['item_id'], "_external": True}
    return(url_for('.get_item', **send_params))

# ------------------------------------------------------------------------------------
@view.route("/get", methods=["GET"])
def get_item():
    """Переход по ссылке для получения объекта
    • GET-запрос (/get)
    • параметры запроса – ссылка, сгенерированная в ответе метода send и временный токен принимающего пользователя, для проверки принадлежности

    Returns:
        str: сообщение об успешном получении объекта
    """

    data = request.json

    # сначала проверяем токены принимающего пользователя - в параметрах ссылки, и в json-body
    i_am = User.check_user_token(data['token'])
    if i_am is None:
        return('ERROR: User does not exist or token expired. Try to login again')

    receiver = User.check_user_token(request.args['to'])
    if receiver is None:
        return('ERROR: User-receiver does not exist or token expired. Try to login again')

    if i_am.id != receiver.id:
    # if to != data['token']:
        return('ERROR: You are not receiver of this item')

    # собственно передача предмета
    try:
        Item.change_owner(request.args['item'], receiver.id)
        # Item.change_owner(item, receiver.id)
        return(f'OK Item sent to {receiver.login}')
    except Exception as ex:
        return(f'ERROR, Item [{request.args["item"]}] was not sent: {ex.args[0]}')
        # return(f'ERROR, Item [{item}] was not sent: {ex.args[0]}')

